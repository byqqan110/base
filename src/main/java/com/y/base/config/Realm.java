package com.y.base.config;

import com.y.base.model.Account;
import com.y.base.service.AccountService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import javax.annotation.Resource;

public class Realm extends AuthorizingRealm {

    @Resource
    private AccountService accountService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
//        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        //获得当前用户
//        Subject subject = SecurityUtils.getSubject();
//        Account account = (Account) subject.getPrincipal();


        return null;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        // 1.获取用户输入的账户信息
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;

        // 模拟数据库的密码
//        String name = "jack";
//        String password = "1234";
//
//        if (!token.getUsername().equals(name)) {
//            // 用户不存在
//            return null;
//        }
//
//        Account account = new Account();
//        account.setUsername(name);
//        account.setPassword(password);

        Account account = accountService.findByName(token.getUsername());

        if (account == null) {
            // 用户不存在
            return null;
        }

        return new SimpleAuthenticationInfo(account, account.getPassword(), "");
    }
}
