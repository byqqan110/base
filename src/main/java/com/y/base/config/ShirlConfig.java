package com.y.base.config;

import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * shrio配置类
 */
@Configuration
public class ShirlConfig {

    /**
     * 创建ShiroFilterFactoryBean
     *
     * @return
     */
    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(DefaultWebSecurityManager securityManager) {
        ShiroFilterFactoryBean bean = new ShiroFilterFactoryBean();
        //关联SecurityManager
        bean.setSecurityManager(securityManager);

        Map<String, String> filterMap = new LinkedHashMap<>();
        //认证过滤器
        //授权过滤器
        filterMap.put("/account/addPerms", "perms[add]");
        filterMap.put("/account/updatePerms", "perms[update]");

        //不拦截登录操作
        filterMap.put("/index", "anon");
        filterMap.put("/login", "anon");
        filterMap.put("/loginFtl", "anon");
        filterMap.put("/**", "authc");
        //添加account过滤器      index的请求只要使用rememberme的功能就无需拦截
        filterMap.put("/index","user");

        //添加shiro过滤器
        bean.setFilterChainDefinitionMap(filterMap);

        //修改登录请求页面
        bean.setLoginUrl("/index");

        //添加未授权提示页面
        bean.setUnauthorizedUrl("/unAuth");
        return bean;
    }

    /**
     * 创建SecurityManager
     *
     * @return
     */
    @Bean
    public DefaultWebSecurityManager defaultSecurityManager(Realm realm, CookieRememberMeManager rememberMeManager) {
        DefaultWebSecurityManager manager = new DefaultWebSecurityManager();
        //关联realm
        manager.setRealm(realm);
        //关联rememberMeManager
        manager.setRememberMeManager(rememberMeManager);
        return manager;
    }

    /**
     * CookieRememberMeManager
     *
     * @return
     */
    @Bean
    public CookieRememberMeManager CookieRememberMeManager(SimpleCookie cookie) {
        CookieRememberMeManager manager = new CookieRememberMeManager();
        manager.setCookie(cookie);
        return manager;
    }

    /**
     * RememberMe功能
     *
     * @return
     */
    @Bean
    public SimpleCookie simpleCookie() {
        SimpleCookie simpleCookie = new SimpleCookie("rememberMe");
        //设置cookie时间长度
        simpleCookie.setMaxAge(60 * 60 * 24 * 3);
        //设置只读模型
        simpleCookie.setHttpOnly(true);
        return simpleCookie;
    }

    /**
     * 创建Realm
     *
     * @return
     */
    @Bean
    public Realm realm() {
        Realm realm = new Realm();

        return realm;
    }
}
