package com.y.base.controller;

import com.y.base.model.Account;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class LoginController {

    @RequestMapping(value = "/index")
    public ModelAndView index(ModelAndView mv) {
        mv.setViewName("index");
        return mv;
    }

    @RequestMapping(value = "/login")
    public ModelAndView login(ModelAndView mv) {
        mv.setViewName("login");
        return mv;
    }

    @RequestMapping(value = "/loginFtl", method = RequestMethod.POST)
    public ModelAndView toLogin(ModelAndView mv, Account account, HttpServletRequest request, String rememberMe, String code) {
        //判断验证码是否正确
        if (!StringUtils.isEmpty(code)) {
            //取出验证码
            String verifyCode = (String) request.getSession().getAttribute("verifyCode");
            if (!code.equals(verifyCode)) {
                mv.addObject("msg", "验证码错误");
                mv.setViewName("/login");
                return mv;
            }
        }

        //使用shiro进行登录
        Subject subject = SecurityUtils.getSubject();
//        AuthenticationToken token = new UsernamePasswordToken(account.getUsername(), account.getPassword());

        //使用shiro md5进行加密
        Md5Hash hash = new Md5Hash(account.getPassword(), account.getUsername(), 3);

        UsernamePasswordToken token = new UsernamePasswordToken(account.getUsername(), hash.toString());

        //设置rememberme的功能
        if (rememberMe != null) {
            token.setRememberMe(true);
        }

        try {
            subject.login(token);

            Account dbAccount = (Account) subject.getPrincipal();

            request.getSession().setAttribute("userName", account.getUsername());

            mv.setViewName("redirect:/index");
            return mv;
        } catch (UnknownAccountException e) {
            mv.addObject("msg", "用户名不存在");
            mv.setViewName("login");
            return mv;
        } catch (IncorrectCredentialsException e) {
            mv.addObject("msg", "密码错误");
            mv.setViewName("login");
            return mv;
        }
    }

    @RequestMapping(value = "/loginHtml")
    public String loginHtml() {
        return "/login";
    }

    @RequestMapping(value = "/logout")
    public ModelAndView logout(ModelAndView mv) {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        mv.setViewName("redirect:/login");
        return mv;
    }
}
