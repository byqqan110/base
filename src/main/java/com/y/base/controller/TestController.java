package com.y.base.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

@Controller
public class TestController {

    /**
     * @param model
     * @return
     */
    @RequestMapping("/testThymeleaf")
    public String testThymeleaf(Model model) {
        model.addAttribute("now", new Date());
        return "/testThymeleaf";
    }

    @RequestMapping("/testFreemarker")
    public ModelAndView testFreemarker(ModelAndView mv) {
        mv.addObject("now", "111");
        mv.setViewName("testFreemarker");
        return mv;
    }
}
