package com.y.base.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller(value = "/account")
public class AccountController {

    @RequestMapping(value = "/list")
    public String list() {
        return "list";
    }

    @RequestMapping(value = "/addPerms")
    public String addPerms() {
        return "addPerms";
    }

    @RequestMapping(value = "/updatePerms")
    public String updatePerms() {
        return "updatePerms";
    }

}
