package com.y.base.service;

import com.y.base.model.Account;

public interface AccountService {

    Account findByName(String name);

}
