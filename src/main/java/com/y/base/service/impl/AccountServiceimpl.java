package com.y.base.service.impl;

import com.y.base.dao.AccountMapper;
import com.y.base.model.Account;
import com.y.base.service.AccountService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
@Transactional
public class AccountServiceimpl implements AccountService {

    @Resource
    private AccountMapper accountMapper;

    @Override
    public Account findByName(String name) {
        Account account = accountMapper.findByName(name);
        return account;
    }
}
