package com.y.base.dao;

import com.y.base.model.RolePermission;

public interface RolePermissionMapper {
    int deleteByPrimaryKey(Short id);

    int insert(RolePermission record);

    int insertSelective(RolePermission record);

    RolePermission selectByPrimaryKey(Short id);

    int updateByPrimaryKeySelective(RolePermission record);

    int updateByPrimaryKey(RolePermission record);
}