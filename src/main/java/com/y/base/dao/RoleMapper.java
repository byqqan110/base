package com.y.base.dao;

import com.y.base.model.Role;

public interface RoleMapper {
    int deleteByPrimaryKey(Short id);

    int insert(Role record);

    int insertSelective(Role record);

    Role selectByPrimaryKey(Short id);

    int updateByPrimaryKeySelective(Role record);

    int updateByPrimaryKey(Role record);
}