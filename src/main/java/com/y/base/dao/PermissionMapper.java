package com.y.base.dao;

import com.y.base.model.Permission;

public interface PermissionMapper {
    int deleteByPrimaryKey(Short id);

    int insert(Permission record);

    int insertSelective(Permission record);

    Permission selectByPrimaryKey(Short id);

    int updateByPrimaryKeySelective(Permission record);

    int updateByPrimaryKey(Permission record);
}